/* Dropdown */
let dropdownDiv = document.querySelector('.dropdown');
let buttonDrop = document.querySelector('.menu-header-mob');
let title = document.querySelector('.title');
let subtitle = document.querySelector('.subtitle-header');

buttonDrop.addEventListener('click', () => {
    if (dropdownDiv.style.display === 'block') {
        dropdownDiv.style.display = 'none';
        title.style.opacity = 1;
        subtitle.style.opacity = 1;
    } else {
        dropdownDiv.style.display = 'block';
        title.style.opacity = 0.2;
        subtitle.style.opacity = 0.2;
    }

});



/*Carousel*/

let buttonNext = document.querySelector('.btn-next');
let buttonBack = document.querySelector('.btn-back');
let list = document.querySelector('.list');



buttonNext.addEventListener('click', () => {

    let margin = parseInt(list.style.marginLeft);
    if (margin > -300) {
        let newMargin = margin - 100;
        list.style.marginLeft = newMargin + 'vw';
    }

});

buttonBack.addEventListener('click', () => {
    ;
    let margin = parseInt(list.style.marginLeft);
    if (margin < 0) {
        let newMargin = margin + 100;
        list.style.marginLeft = newMargin + 'vw';
    }

});
/* /Carousel */



let filterSelectEl = document.getElementById('filter');
let itemsEl = document.getElementById('items');


filterSelectEl.onchange = function () {
    console.log(this.value);
    let items = itemsEl.getElementsByClassName('gray-layer');
    for (let i = 0; i < items.length; i++) {
        if (items[i].classList.contains(this.value)) {
            items[i].style.display = 'block';
        } else {
            items[i].style.display = 'none';
        }
    }
};





/* TABS */
const buttonsContainer = document.querySelector('.buttons');
const tabs = document.querySelector('.tabs');

buttonsContainer.addEventListener('click', event => {
    let index = event.target.closest('.button').dataset.value;

    tabs.querySelector('.active').classList.remove('active');
    tabs.querySelector(`.tab--${index}`).classList.add('active');
});

/* /TABS */

/* VALIDATE EMAIL AND NAME */
function validateEmail() {
    let name = document.getElementById('name').value;
    if (name.length <= 1) {
        alert('Укажите имя более одного символа');
        return false;
    }

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var address = document.forms[form_id].elements[email].value;
    if (reg.test(address) == false) {
        alert('Введите корректный e-mail');
        return false;
    }
}
/* /VALIDATE EMAIL AND NAME */


/* MAP */
let text = document.querySelector('.inner-text');
let blackBg = document.querySelector('.conteiner-map');
blackBg.addEventListener("click", function () {
    this.classList.toggle("is-active");
    text.innerHTML = (text.innerHTML == 'close the map') ? 'open the map' : 'close the map';
    
});



 
/* /MAP */